module gitlab.com/brandon.kauffman/gitlab-ci-notify-slack-changed-state

go 1.19

require (
	github.com/sirupsen/logrus v1.9.0
	github.com/slack-go/slack v0.12.1
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
