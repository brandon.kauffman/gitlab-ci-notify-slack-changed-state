package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"gopkg.in/alecthomas/kingpin.v2"
)

var failure = kingpin.Flag("failure", "Use this flag on failure condition").Bool()

type Pipeline struct {
	Status   string `json:"status"`
	IID      int    `json:"iid"`
	ID       int    `json:"id"`
	Ref      string `json:"ref"`
	Sha      string `json:"sha"`
	SelfLink string `json:"web_url"`
	User     User   `json:"user,omitempty"`
}
type User struct {
	Username  string `json:"username"`
	Name      string `json:"name"`
	SelfLink  string `json:"web_url"`
	AvatarURL string `json:"avatar_url"`
}

func main() {
	kingpin.Parse()
	base := os.Getenv("CI_SERVER_URL")
	project := os.Getenv("CI_PROJECT_ID")
	token := os.Getenv("PIPELINE_API_ACCESS_TOKEN")
	pipelineID := os.Getenv("CI_PIPELINE_ID")
	slackWebhook := os.Getenv("SLACK_WEBHOOK")
	slackChannel := os.Getenv("SLACK_CHANNEL")
	projectName := os.Getenv("CI_PROJECT_NAME")
	pipelineURL := fmt.Sprintf("%s/api/v4/projects/%s/pipelines", base, project)
	lastPipline := ""
	pipelines, err := getPipelines(pipelineURL, token)
	if err != nil {
		logrus.Fatal(err)
	}
	pipeline, err := getPipeline(pipelineURL+"/"+pipelineID, token)
	if err != nil {
		logrus.Error(err)
	}
	projectURL := fmt.Sprintf("%s/-/commits/", os.Getenv("CI_PROJECT_URL"))
	for _, v := range pipelines {
		fmt.Println(v.IID, v.ID, v.Status)
	}
	for _, v := range pipelines {
		fmt.Println(v.IID, v.Status)
		if (v.Status == "failed" || v.Status == "success") && v.IID < pipeline.IID {
			lastPipline = v.Status
			break
		}
	}
	if *failure && lastPipline == "success" {
		sendSlackWebhook(*failure, slackChannel, slackWebhook, projectName, pipeline, projectURL)

	} else if lastPipline == "failed" && !*failure {
		sendSlackWebhook(*failure, slackChannel, slackWebhook, projectName, pipeline, projectURL)
	}
}

func getPipelines(url string, token string) ([]Pipeline, error) {
	var pipelines []Pipeline
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()
	req.Header.Add("Accept", "application/json")
	req.Header.Add("PRIVATE-TOKEN", token)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&pipelines)
	if err != nil {
		bodyBytes, err2 := io.ReadAll(resp.Body)
		if err2 != nil {
			logrus.Error(err2)
		}
		logrus.Error(resp.StatusCode, " RESP: ", string(bodyBytes))

		return nil, err
	}
	return pipelines, nil
}

func getPipeline(url string, token string) (Pipeline, error) {
	var pipeline Pipeline
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return pipeline, err
	}
	q := req.URL.Query()
	req.URL.RawQuery = q.Encode()
	req.Header.Add("Accept", "application/json")
	req.Header.Add("PRIVATE-TOKEN", token)
	resp, err := client.Do(req)
	if err != nil {
		return pipeline, err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(&pipeline)
	if err != nil {
		bodyBytes, err2 := io.ReadAll(resp.Body)
		if err2 != nil {
			logrus.Error(err2)
		}
		logrus.Error(resp.StatusCode, " RESP: ", string(bodyBytes))

		return pipeline, err
	}
	return pipeline, nil
}

func sendSlackWebhook(failure bool, channel string, webhook string, projectName string, pipeline Pipeline, baseUrl string) {
	var color string
	var text string
	if failure {
		color = "#8a0000"
		text = fmt.Sprintf("<%s|*Pipeline failed: #%d*>", pipeline.SelfLink, pipeline.IID)

	} else {
		color = "#058a00"
		text = fmt.Sprintf("<%s|*Pipeline fixed: #%d*>", pipeline.SelfLink, pipeline.IID)

	}
	attachment := slack.Attachment{
		Color:      color,
		Fallback:   text,
		AuthorName: fmt.Sprintf("%s (%s)", pipeline.User.Name, pipeline.User.Username),
		AuthorIcon: pipeline.User.AvatarURL,
		AuthorLink: pipeline.User.SelfLink,
		Text:       text,
		Footer:     projectName,
		Ts:         json.Number(strconv.FormatInt(time.Now().Unix(), 10)),
		Fields:     []slack.AttachmentField{{Title: "Commit", Short: true, Value: fmt.Sprintf("<%s%s | %s>", baseUrl, pipeline.Sha, pipeline.Sha)}, {Title: "Ref", Short: true, Value: fmt.Sprintf("<%s%s|%s>", baseUrl, pipeline.Ref, pipeline.Ref)}},
	}
	msg := slack.WebhookMessage{
		IconURL:     "https://w7.pngwing.com/pngs/535/202/png-transparent-gitlab-logo-version-control-company-react-others-miscellaneous-angle-company-thumbnail.png",
		Attachments: []slack.Attachment{attachment},
		Username:    "Gitlab",
		Channel:     channel,
	}

	err := slack.PostWebhook(webhook, &msg)
	if err != nil {
		logrus.Fatal(err)
	}

}
